package projekt;

import java.util.SplittableRandom;

public class Event {
    public int type=-1; //1-TOR,2-ECKE,3-GELBEKARTE,4-ROTEKARTE,5-GELBROTEKARTE,6-ELFMETER,7-AUSWECHSELUNG, 8-TORANNULIERUNG
    public String besonderheit="";
    public int timeStamp; //ZeitPunkt des Events
    public String spielerName="Unbekannter Spieler";
    public int spielerNummer=-1; // if <0 -> keine Nummer eingegeben
    public int spielerName2="Unbekannter Spieler";
    public int spielerNummer2=-1;
    public boolean team; //True-> Heim, False->Gast

    public void setTimeStamp(int zeitMin){
        timeStamp = zeitMin+1;
    }
    public String spielerNrString(int nr){
        String s="";
        if(nr>0){
        s= " (#" + Integer.toString(nr) + ")";
        }
        return s;
    }    

}
