package projekt;
        
        import javafx.animation.Animation;
        import javafx.animation.KeyFrame;
        import javafx.animation.Timeline;
        import javafx.application.Platform;
        import javafx.event.ActionEvent;
        import javafx.event.EventHandler;
        import javafx.fxml.FXML;
        import javafx.fxml.FXMLLoader;
        import javafx.geometry.Insets;
        import javafx.scene.Scene;
        import javafx.scene.control.*;
        import javafx.scene.input.KeyEvent;
        import javafx.scene.layout.AnchorPane;
        import javafx.scene.layout.Background;
        import javafx.scene.layout.BackgroundFill;
        import javafx.scene.layout.CornerRadii;
        import javafx.scene.paint.Color;
        import javafx.scene.text.Font;
        import javafx.stage.Stage;
        import javafx.stage.WindowEvent;
        import javafx.util.Duration;
        
        import java.io.IOException;
        import java.time.LocalDateTime;
        import java.time.format.DateTimeFormatter;
        
        import static java.lang.Integer.parseInt;

public class Controller1{

    private int aktuellerTab = 1;
    Team heim = new Team();
    Team gast = new Team();
    Stadion stadion = new Stadion();
    private final Stage thisStage;
    private Controller2 controller2 = new Controller2(this);

    public Controller1() {
        thisStage = new Stage();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Steuerung.fxml"));
            loader.setController(this);
            thisStage.setScene(new Scene(loader.load()));
            thisStage.setTitle("Steuerung");
             thisStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(javafx.stage.WindowEvent event) {
                    Platform.exit();
                    System.exit(0);
                }
            });
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public void showStage(){ thisStage.show();}
    
    private void openAnzeige(){controller2.showStage();}

    //Funktionszuweisungen
    @FXML private void initialize() {
        b_AnzeigeÖffnen.setOnAction(e -> openAnzeige());
        b_Ecke_Eingabe.setOnAction(e -> controller2.setTab(2));
        b_Startseite.setOnMouseClicked(e -> setSteuerungsTab(b_Startseite, aktuellerTab, t_Startseite));
        b_GelbeKarte.setOnMouseClicked(e -> setSteuerungsTab(b_GelbeKarte , aktuellerTab, t_GelbeKarte));
        b_RoteKarte.setOnMouseClicked(e -> setSteuerungsTab(b_RoteKarte, aktuellerTab, t_RoteKarte));
        b_Nachspielzeit.setOnMouseClicked(e -> setSteuerungsTab(b_Nachspielzeit,aktuellerTab,t_Nachspielzeit));
        b_Auswechselung.setOnMouseClicked(e -> setSteuerungsTab(b_Auswechselung,aktuellerTab,t_Auswechselung));
        b_Ecke.setOnMouseClicked(e -> setSteuerungsTab(b_Ecke, aktuellerTab,t_Ecke));
        b_Tor.setOnMouseClicked(e -> setSteuerungsTab(b_Tor,aktuellerTab,t_Tor));
        b_Elfmeter.setOnMouseClicked(e -> setSteuerungsTab(b_Elfmeter,aktuellerTab,t_Elfmeter));
        b_EventList.setOnMouseClicked(e -> setSteuerungsTab(b_EventList, aktuellerTab, t_EventList));
        b_MAuswechselung.setOnAction(e -> setSteuerungsTab(b_Auswechselung,aktuellerTab,t_Auswechselung));
        b_MEcke.setOnAction(e -> setSteuerungsTab(b_Ecke,aktuellerTab,t_Ecke));
        b_MElfmeter.setOnAction(e -> setSteuerungsTab(b_Elfmeter,aktuellerTab,t_Elfmeter));
        b_MGelbeKarte.setOnAction(e-> setSteuerungsTab(b_GelbeKarte,aktuellerTab,t_GelbeKarte));
        b_MRoteKarte.setOnAction(e -> setSteuerungsTab(b_RoteKarte,aktuellerTab,t_RoteKarte));
        b_MStartseite.setOnAction(e -> setSteuerungsTab(b_Startseite,aktuellerTab,t_Startseite));
        b_MTor.setOnAction(e -> setSteuerungsTab(b_Tor,aktuellerTab,t_Tor));
        b_MNachspielzeit.setOnAction(e -> setSteuerungsTab(b_Nachspielzeit,aktuellerTab,t_Nachspielzeit));
        initClock();
        disableButtons(true);
        b_SpielVorbereitung.setOnAction(e -> dp_SpielVb.setVisible(true));
        dp_SpielVb.getButtonTypes().setAll(ButtonType.FINISH, ButtonType.CANCEL);
        Button finishButton = (Button)dp_SpielVb.lookupButton(ButtonType.FINISH);
        finishButton.setText("Fertig");
        finishButton.setOnAction(e -> dpFinish());
        Button cancelButton = (Button)dp_SpielVb.lookupButton(ButtonType.CANCEL);
        cancelButton.setOnAction(e -> dpCancel());
        b_TimerStarten.setOnAction(e -> startTimer());
        b_TimerAnhalten.setOnAction(e -> stopTimer());
        b_TM_Zurückspulen.setOnAction(e -> editTimerNeg());
        b_TM_Vorspulen.setOnAction(e -> editTimerPos());
        tf_TM_Minuten.setOnKeyTyped(e -> processKeyEvent(e,tf_TM_Minuten));
        tf_AA_Spielernummer.setOnKeyTyped(e -> processKeyEvent(e,tf_AA_Spielernummer));
        tf_AE_Spielernummer.setOnKeyTyped(e -> processKeyEvent(e,tf_AE_Spielernummer));
        tf_GKSpielernummer.setOnKeyTyped(e -> processKeyEvent(e,tf_GKSpielernummer));
        tf_RKSpielernummer.setOnKeyTyped(e -> processKeyEvent(e,tf_RKSpielernummer));
        tf_Nachspielzeit_Länge.setOnKeyTyped(e -> processKeyEvent(e,tf_Nachspielzeit_Länge));
        tf_TorSpielernummer.setOnKeyTyped(e -> processKeyEvent(e,tf_TorSpielernummer));
        sv_Zuschauerzahl.setOnKeyTyped(e -> processKeyEvent(e,sv_Zuschauerzahl));
        b_Tor_Eingabe.setOnAction(e -> torEvent());
        b_Ecke_Eingabe.setOnAction(e -> eckeEvent());
        b_GK_Eingabe.setOnAction(e -> gelbeKarteEvent());
        b_RK_Eingabe.setOnAction(e -> roteKarteEvent());
        b_A_Eingabe.setOnAction(e -> auswechselungEvent());
        b_El_Eingabe.setOnAction(e -> elfmeterEvent());
        b_TorAnnulierung_Eingabe.setOnAction(e -> torAnnulierungEvent());
        b_Abseits_Gast.setOnAction(e -> abseitsZählen(false));
        b_Abseits_Heim.setOnAction(e -> abseitsZählen(true));
        b_Abseits_Annulierung.setOnAction(e -> abseitsAnnulierung());
        b_Reset.setOnAction(e -> resetAll());
    }
    
        @FXML private void dpCancel(){
        sv_StadionName.clear();
        sv_HeimName.clear();
        sv_GastName.clear();
        sv_SchiriName.clear();
        sv_Zuschauerzahl.clear();
        sv_AbseitsZählen.setSelected(false);
        sv_TestModus.setSelected(false);
        dp_SpielVb.setVisible(false);
        AP_LinkesMenü.setDisable(false);
        TP_AktuellesMenü.setDisable(false);
        b_Menü_Fenster.setDisable(false);
        b_Menü_Menü.setDisable(false);
    }

    @FXML private void dpFinish(){
        openAnzeige()
        
        if (!sv_StadionName.getText().equals("")){
        stadion.name = sv_StadionName.getText();
        l_StadionName.setText(stadion.name);
        controller2.setStadionname(stadion.name);
        sv_StadionName.clear();
        }
        if (!sv_Zuschauerzahl.getText().equals("")){
            stadion.zuschauerZahl = parseInt(sv_Zuschauerzahl.getText());
            sv_Zuschauerzahl.clear();
        }
        if (!sv_SchiriName.getText().equals("")){
            stadion.schiedsrichterName = sv_SchiriName.getText();
            sv_SchiriName.clear();
        }
        if (!sv_HeimName.getText().equals("")){
        heim.name = sv_HeimName.getText();
        sv_HeimName.clear();;
        }else{
            heim.name="Heim";
        }
        if (!sv_GastName.getText().equals("")) {
            gast.name = sv_GastName.getText();
            sv_GastName.clear();
        }else{
            gast.name="Gast";
        }
         if (sv_AbseitsZählen.isSelected()){
            b_Abseits_Heim.setVisible(true);
            b_Abseits_Gast.setVisible(true);
            b_Abseits_Annulierung.setVisible(true);
            sv_AbseitsZählen.setSelected(false);
        }
        if (sv_TestModus.isSelected()==true){
            b_TimerAnhalten.setVisible(true);
            b_TM_Vorspulen.setVisible(true);
            b_TM_Zurückspulen.setVisible(true);
            tf_TM_Minuten.setVisible(true);
            sv_TestModus.setSelected(false);
        }
        textHG();
        dp_SpielVb.setVisible(false);
        b_SpielVorbereitung.setDisable(true);
        b_TimerStarten.setDisable(false);
        b_TorAnnulierung_Eingabe.setDisable(true);
        AP_LinkesMenü.setDisable(false);
        TP_AktuellesMenü.setDisable(false);
        b_Menü_Fenster.setDisable(false);
        b_Menü_Menü.setDisable(false);
    }
    
    private  void disableButtons(Boolean b){ // Alle Buttons außer Spielvorbereitung
        b_TimerStarten.setDisable(b);
        b_Abseits_Gast.setDisable(b);
        b_Abseits_Heim.setDisable(b);
        b_Tor_Eingabe.setDisable(b);
        b_TorAnnulierung_Eingabe.setDisable(b);
        b_GK_Eingabe.setDisable(b);
        b_A_Eingabe.setDisable(b);
        b_Ecke_Eingabe.setDisable(b);
        b_El_Eingabe.setDisable(b);
        b_Nachspielzeit_Eingabe.setDisable(b);
        b_RK_Eingabe.setDisable(b);
    }
    
    private void initClock() {

        Timeline clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss - dd.MM.yyyy");
            l_NormaleZeit.setText(LocalDateTime.now().format(formatter));
        }), new KeyFrame(Duration.seconds(1)));
        clock.setCycleCount(Animation.INDEFINITE);
        clock.play();
    }

     @FXML private void setSteuerungsTab(Label l, Integer i ,Tab t){

        switch (i) {
            case 1: b_Startseite.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));break;
            case 2: b_Tor.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));break;
            case 3: b_GelbeKarte.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY))); break;
            case 4: b_RoteKarte.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));break;
            case 5: b_Auswechselung.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));break;
            case 6: b_Ecke.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));break;
            case 7: b_Elfmeter.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));break;
            case 8: b_Nachspielzeit.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));break;
            case 9: b_EventList.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));break;
            default: System.out.println("Fehler: invalider Funktionsaufruf von 'setSteuerungsTab'!");break;
        }
        TP_AktuellesMenü.getSelectionModel().select(t);
        l.setBackground(new Background(new BackgroundFill(Color.LIGHTGREY, CornerRadii.EMPTY, Insets.EMPTY)));
        switch (l.getId()) {
            case "b_Startseite": aktuellerTab=1; break;
            case "b_Tor": aktuellerTab=2; break;
            case "b_GelbeKarte": aktuellerTab=3; break;
            case "b_RoteKarte": aktuellerTab=4; break;
            case "b_Auswechselung": aktuellerTab=5; break;
            case "b_Ecke": aktuellerTab=6; break;
            case "b_Elfmeter": aktuellerTab=7; break;
            case "b_Nachspielzeit": aktuellerTab=8;break;
            case "b_EventList": aktuellerTab=9; break;
            default:System.out.println("Fehler: invalider Funktionsaufruf von 'setSteuerungsTab'!");break;
        }
    }
    
    //SpielTimer
    int zeitSek = 0 , zeitMin = 0;
    double pbProgress = 0.00;
    
     @FXML public void updateTimer() {
        if (zeitSek == 59) {
            zeitMin++;
            zeitSek = 0;
            pbProgress += 1.00 / 90;
            pb_Spielfortschritt.setProgress(pbProgress);
        } else zeitSek++;
       printTimer();
    }

   Timeline timer = new Timeline(
            new KeyFrame(Duration.seconds(1), event -> updateTimer())
    );

    public void startTimer(){
        timer.setCycleCount(Animation.INDEFINITE);
        timer.play();
        b_TimerStarten.setDisable(true);
        b_TimerAnhalten.setDisable(false);
        disableButtons(false);
    }

    public void stopTimer(){
        timer.stop();
        b_TimerStarten.setDisable(false);
        b_TimerAnhalten.setDisable(true);
    }

    public void printTimer(){
        if (zeitMin < 10) {
            if (zeitSek < 10) {
                l_Timer.setText("0" + zeitMin + ":0" + zeitSek);
                l_Spielzeit.setText("0" + zeitMin + ":0" + zeitSek);
                controller2.setTimer("0"+ zeitMin + ":0" + zeitSek);
            } else {
                l_Timer.setText("0" + zeitMin + ":" + zeitSek);
                l_Spielzeit.setText("0" + zeitMin + ":" + zeitSek);
                controller2.setTimer("0" + zeitMin + ":" + zeitSek);
            }
        } else{
            if (zeitSek < 10) {
                l_Timer.setText("" + zeitMin + ":0" + zeitSek);
                l_Spielzeit.setText("" + zeitMin + ":0" + zeitSek);
                controller2.setTimer(""+ zeitMin + ":0" + zeitSek);
            } else {
                l_Timer.setText("" + zeitMin + ":" + zeitSek);
                l_Spielzeit.setText("" + zeitMin + ":" + zeitSek);
                controller2.setTimer("" + zeitMin + ":" + zeitSek);
            }
        }
    }

    public void editTimerNeg(){
        if (!tf_TM_Minuten.getText().equals("")){
         if ((zeitMin-(parseInt(tf_TM_Minuten.getText()))>0)){
                zeitMin -= (parseInt (tf_TM_Minuten.getText()));
            }else zeitMin= 0;
        pb_Spielfortschritt.setProgress(zeitMin*1.00/90);
        tf_TM_Minuten.clear();
        printTimer();
        }
    }
    
    public void editTimerPos(){
        if (!tf_TM_Minuten.getText().equals("")){
         if ((zeitMin+(parseInt(tf_TM_Minuten.getText()))<90)){
                zeitMin += (parseInt(tf_TM_Minuten.getText()));
            }else zeitMin= 90;
        pb_Spielfortschritt.setProgress(zeitMin*1.00/90);
        tf_TM_Minuten.clear();
        printTimer();
        }
    }

//Erlaubt nur noch Int
    @FXML public void processKeyEvent(KeyEvent ev, TextField tf) {
        if (ev.getCharacter().matches("[0-9]")) {
            return;
        }
        ev.consume();
        tf.backward();
        tf.deleteNextChar();
    }
    
    @FXML
    void AnzeigeSchließen(ActionEvent event){
        Platform.exit();
        System.exit(0);
        System.out.println("App geschlossen");
    }

    int evNr = 0;
    boolean letztesTor; // true -> heim, false-> gast
    Event[] event = new Event[100];
    
    @FXML void torEvent(){
        if ((CB_Heim.isSelected()||CB_Gast.isSelected())&&(zeitSek>0||zeitMin>0)){
        event[evNr] = new Event();
        event[evNr].type=1;
        event[evNr].team = CB_Heim.isSelected();
        event[evNr].setTimeStamp(zeitMin);
        if(!tf_TorSpielernummer.getText().matches("")){
            event[evNr].spielerNummer = parseInt(tf_TorSpielernummer.getText());
        }
            if(!tf_TorSpielername.getText().equals("")){
                event[evNr].spielerName = tf_TorSpielername.getText();
            }else{
                event[evNr].spielerName = ""; // keine Nameneingegeben
            }

        if(CB_Heim.isSelected()){
            heim.tore++;
            letztesTor=true;
        }else{
            gast.tore++;
            letztesTor=false;
        }
        //Prüfung ob Tor nach Elfmeter Elfmeter
        if(evNr>0){
            if(event[evNr-1].type==6) {
                if(event[evNr-1].team== event[evNr].team && event[evNr-1].timeStamp==event[evNr].timeStamp){
                    event[evNr].besonderheit="(EF)";
                }    
            }
        }

            tf_TorSpielername.clear();
            tf_TorSpielernummer.clear();
            CB_Gast.setSelected(false);
            CB_Heim.setSelected(false);
            printTor();
            printEvent(evNr);
        }
    }

    @FXML public void printTor(){
            l_Spielstand1.setText(heim.tore + ":" + gast.tore);
            l_Spielstand.setText(heim.tore + ":" + gast.tore);
            controller2.setSpielstand(heim.tore + ":" + gast.tore);
    }

    @FXML public void printEvent(int i){
        System.out.println("Event of type (" + event[i].type + ") added");
        l_TickerZeit.setText(l_TickerZeit.getText() + event[i].timeStamp+ "'\n");
        
        if (event[i].team){
            switch (event[i].type){
                case 1:l_TickerHInfo.setText(l_TickerHInfo.getText() + heim.tore + ":" + gast.tore +" "+event[i].besonderheit + " " + event[i].spielerName +  event[i].spielerNrString(event[i].spielerNummer) + "\n");
                        break;
                case 2:l_TickerHInfo.setText(l_TickerHInfo.getText() + heim.ecken+ ". Ecke\n");
                        break;
                case 3:l_TickerHInfo.setText(l_TickerHInfo.getText() + "Gelbe Karte für "+ event[i].spielerName + event[i].spielerNrString(event[i].spielerNummer) + "\n");
                        break;
                case 4:l_TickerHInfo.setText(l_TickerHInfo.getText() + "Rote Karte für "+ event[i].spielerName + event[i].spielerNrString(event[i].spielerNummer) + "\n");
                        break;
                case 5:l_TickerHInfo.setText(l_TickerHInfo.getText() + "Gelb/Rot für "+ event[i].spielerName +  event[i].spielerNrString(event[i].spielerNummer) + "\n");
                        break;
                case 6:l_TickerHInfo.setText(l_TickerHInfo.getText() + "Elfmeter\n");
                        break;
                case 7:
                    l_TickerHInfo.setText(l_TickerHInfo.getText() + "Auswechselung:\t ↓: " + event[i].spielerName  + event[i].spielerNrString(event[i].spielerNummer) + "\n \t \t \t \t ↑: "+ event[i].spielerName2 + event[i].spielerNrString(event[i].spielerNummer2) +"\n");
                    l_TickerGInfo.setText(l_TickerGInfo.getText()+"\n");
                    l_TickerZeit.setText(l_TickerZeit.getText()+"\n");
                        break;
                case 8:l_TickerHInfo.setText(l_TickerHInfo.getText() + "Annulierung des Tores von "+ event[i-1].spielerName + event[i].spielerNrString(event[i-1].spielerNummer) + "\n");
                        break;
                default:System.out.println("Fehlerhafter Eventtyp ("+ i +")");    
                        break;
            }
            l_TickerGInfo.setText(l_TickerGInfo.getText()+"\n");

        }else {
            switch (event[i].type) {
                case 1:
                   l_TickerGInfo.setText(l_TickerGInfo.getText() + heim.tore + ":" + gast.tore + " " + event[i].besonderheit + " " + event[i].spielerName + event[i].spielerNrString(event[i].spielerNummer) + "\n");
                   break;
                case 2:
                    l_TickerGInfo.setText(l_TickerGInfo.getText() + gast.ecken + ". Ecke\n");
                    break;
                case 3:
                    l_TickerGInfo.setText(l_TickerGInfo.getText() + "Gelbe Karte für " + event[i].spielerName + event[i].spielerNrString(event[i].spielerNummer) + "\n");
                    break;
                case 4:
                    l_TickerGInfo.setText(l_TickerGInfo.getText() + "Rote Karte für " + event[i].spielerName + event[i].spielerNrString(event[i].spielerNummer) + "\n");
                    break;
                case 5:
                    l_TickerGInfo.setText(l_TickerGInfo.getText() + "Gelb/Rot für " + event[i].spielerName + event[i].spielerNrString(event[i].spielerNummer) + "\n");
                    break;
                case 6:
                    l_TickerGInfo.setText(l_TickerGInfo.getText() + "Elfmeter\n");
                    break;
                case 7:
                    l_TickerGInfo.setText(l_TickerGInfo.getText() + "Auswechselung:\t ↓: " + event[i].spielerName + event[i].spielerNrString(event[i].spielerNummer) + "\n \t \t \t \t ↑: " + event[i].spielerName2 + event[i].spielerNrString(event[i].spielerNummer2) + "\n");
                    l_TickerHInfo.setText(l_TickerHInfo.getText() + "\n");
                    l_TickerZeit.setText(l_TickerZeit.getText() + "\n");
                    break;
                case 8:
                     l_TickerGInfo.setText(l_TickerGInfo.getText() + "Annulierung des Tores von "+ event[i-1].spielerName + event[i].spielerNrString(event[i-1].spielerNummer) + "\n");
                     break;
                default:
                    System.out.println("Fehlerhafter Eventtyp (" + i + ")");
                    break;
            }
            l_TickerHInfo.setText(l_TickerHInfo.getText() + "\n");
        }
        
        if(event[i].type==1){
            b_TorAnnulierung_Eingabe.setDisable(false);
        }else{
            b_TorAnnulierung_Eingabe.setDisable(true);
        }
        evNr++;
    }

    @FXML public void eckeEvent(){
        if((CB_ECKE_Heim.isSelected()||CB_ECKE_Gast.isSelected())&&(zeitMin>0||zeitSek>0)){
            event[evNr] = new Event();
            event[evNr].setTimeStamp(zeitMin);
            event[evNr].type=2;
            if (CB_ECKE_Heim.isSelected()){
                heim.ecken++;
                CB_ECKE_Heim.setSelected(false);
                event[evNr].team=true;
            }else {
                gast.ecken++;
                CB_ECKE_Gast.setSelected(false);
                event[evNr].team=false;
            }
            printEvent(evNr);
            printEcke();
        }
    }

    @FXML public void printEcke(){
        Timeline wait = new Timeline(
                new KeyFrame(Duration.ZERO, event -> controller2.setTab(2)),
                new KeyFrame(Duration.seconds(5), event -> controller2.setTab(3)),
                new KeyFrame(Duration.seconds(10), event -> controller2.setTab(1))
        );
        wait.setCycleCount(1);
        wait.play();
    }
    
    @FXML public void gelbeKarteEvent(){
        if((CB_GKHeim.isSelected()||CB_GKGast.isSelected())&&(zeitMin>0||zeitSek>0)){
            event[evNr] = new Event();
            event[evNr].setTimeStamp(zeitMin);
            if (CB_GelbRot.isSelected()){
                event[evNr].type=5;
            }else{
                event[evNr].type=3;
            }
            if (CB_GKHeim.isSelected()){
                event[evNr].team=true;
                heim.gelbeKarten++;
            }else {
                event[evNr].team=false;
                gast.gelbeKarten++;
            }
            if (!tf_GKSpielername.getText().equals("")){
                event[evNr].spielerName=tf_GKSpielername.getText();
                tf_GKSpielername.clear();
            }else {
                event[evNr].spielerName="Unbekannter Spieler";
            }
            if (!tf_GKSpielernummer.getText().equals("")){
                event[evNr].spielerNummer= parseInt(tf_GKSpielernummer.getText());
                tf_GKSpielernummer.clear();
            }
            //TODO printGelbeKarte();
            printEvent(evNr);
            CB_GKHeim.setSelected(false);
            CB_GKGast.setSelected(false);
            CB_GelbRot.setSelected(false);
        }
    }

    @FXML public void roteKarteEvent(){
        if((CB_RKHeim.isSelected()||CB_RKGast.isSelected())&&(zeitMin>0||zeitSek>0)){
            event[evNr] = new Event();
            event[evNr].setTimeStamp(zeitMin);
            event[evNr].type=4;
            if (CB_RKHeim.isSelected()){
                event[evNr].team=true;
                heim.roteKarten++;
            }else {
                event[evNr].team=false;
                gast.roteKarten++;
            }
            if (!tf_RKSpielername.getText().equals("")){
                event[evNr].spielerName=tf_RKSpielername.getText();
                tf_RKSpielername.clear();
            }else {
                event[evNr].spielerName="Unbekannter Spieler";
            }
            if (!tf_RKSpielernummer.getText().equals("")){
                event[evNr].spielerNummer= parseInt(tf_RKSpielernummer.getText());
                tf_RKSpielernummer.clear();
            }
            //TODO printRoteKarte();
            printEvent(evNr);
            CB_RKHeim.setSelected(false);
            CB_RKGast.setSelected(false);
        }
    }

    @FXML public void auswechselungEvent(){
        if ((CB_A_Heim.isSelected()||CB_A_Gast.isSelected())&&(zeitSek>0||zeitMin>0)){
            if ((CB_A_Heim.isSelected()&&heim.auswechslungen<3)||(CB_A_Gast.isSelected()&&gast.auswechslungen<3)){
                event[evNr]= new Event();
                event[evNr].setTimeStamp(zeitMin);
                event[evNr].type=7;
                if (CB_A_Heim.isSelected()){
                    event[evNr].team=true;
                    heim.auswechslungen++;
                }else {
                    event[evNr].team=false;
                    gast.auswechslungen++;
                }
                if (!tf_AA_Spielername.getText().equals("")){
                    event[evNr].spielerName = tf_AA_Spielername.getText();
                    tf_AA_Spielername.clear();
                }else {
                    event[evNr].spielerName = "Unbekannt";
                }
                if (!tf_AE_Spielername.getText().equals("")){
                    event[evNr].spielerName2 = tf_AE_Spielername.getText();
                    tf_AE_Spielername.clear();
                }else{
                    event[evNr].spielerName2 = "Unbekannt";
                }
                if (!tf_AA_Spielernummer.getText().equals("")){
                    event[evNr].spielerNummer= parseInt(tf_AA_Spielernummer.getText());
                    tf_AA_Spielernummer.clear();
                }
                if (!tf_AE_Spielernummer.getText().equals("")){
                    event[evNr].spielerNummer2= parseInt(tf_AE_Spielernummer.getText());
                    tf_AE_Spielernummer.clear();
                }

                //TODO printAuswechselung()
                printEvent(evNr);
                CB_A_Gast.setSelected(false);
                CB_A_Heim.setSelected(false);
            }
        }
    }

    @FXML public void elfmeterEvent(){
        if((CB_ElH.isSelected()||CB_ElG.isSelected())&&(zeitMin>0||zeitSek>0)){
            event[evNr] = new Event();
            event[evNr].setTimeStamp(zeitMin);
            event[evNr].type=6;
            if (CB_ElH.isSelected()){
                heim.elfmeter++;
                event[evNr].team=true;
            }else {
                gast.elfmeter++;
                event[evNr].team=false;
            }
            printEvent(evNr);
            //TODO printElfmeter();

            CB_ElH.setSelected(false);
            CB_ElG.setSelected(false);
        }
    }

    @FXML public void torAnnulierungEvent(){
        if (heim.tore>0 || gast.tore>0) {
            event[evNr] = new Event();
            event[evNr].setTimeStamp(zeitMin);
            event[evNr].type=8;
            if (event[evNr - 1].team) {
                heim.tore--;
                event[evNr].team=true;
            } else {
                gast.tore--;
                event[evNr].team=false;
            }
            printEvent(evNr);
            printTor();
        }
    }
    boolean letztesAbseits;
    public void abseitsZählen(Boolean b){
        if(b){
            heim.abseits++;
        } else{
            gast.abseits++;
        }
        letztesAbseits=b;
        b_Abseits_Annulierung.setDisable(false);
    }

    public void abseitsAnnulierung(){
            if (letztesAbseits){
                heim.abseits--;
            }else{
                gast.abseits--;
            }
         b_Abseits_Annulierung.setDisable(true);

    }
    
    //Funktionstätigkeit der CheckBoxes -> so dass immer nur eine aktiv ist
    @FXML
    void CBGedrückt_TorH(ActionEvent event){
        if (CB_Heim.isSelected()==true) CB_Gast.setSelected(false);
    }
    @FXML
    void CBGedrückt_TorG(ActionEvent event){
        if (CB_Gast.isSelected()==true) CB_Heim.setSelected(false);
    }
    @FXML
    void CBGedrückt_GKH(ActionEvent event){
        if (CB_GKHeim.isSelected()==true) CB_GKGast.setSelected(false);
    }
    @FXML
    void CBGedrückt_GKG(ActionEvent event){
        if (CB_GKGast.isSelected()==true) CB_GKHeim.setSelected(false);
    }
    @FXML
    void CBGedrückt_RKH(ActionEvent event){
        if (CB_RKHeim.isSelected()==true) CB_RKGast.setSelected(false);
    }
    @FXML
    void CBGedrückt_RKG(ActionEvent event){
        if (CB_RKGast.isSelected()==true) CB_RKHeim.setSelected(false);
    }
    @FXML
    void CBGedrückt_AH(ActionEvent event){
        if (CB_A_Heim.isSelected()==true) CB_A_Gast.setSelected(false);
    }
    @FXML
    void CBGedrückt_AG(ActionEvent event){
        if (CB_A_Gast.isSelected()==true) CB_A_Heim.setSelected(false);
    }
    @FXML
    void CBGedrückt_EH(ActionEvent event){
        if (CB_ECKE_Heim.isSelected()==true) CB_ECKE_Gast.setSelected(false);
    }
    @FXML
    void CBGedrückt_EG(ActionEvent event){
        if (CB_ECKE_Gast.isSelected()==true) CB_ECKE_Heim.setSelected(false);
    }
    @FXML
    void CBGedrückt_ElH(ActionEvent event){
        if (CB_ElH.isSelected()==true) CB_ElG.setSelected(false);
    }
    @FXML
    void CB_Gedrückt_ElG(ActionEvent event){
        if(CB_ElG.isSelected()==true) CB_ElH.setSelected(false);
    }
    
    @FXML
    void textHG(){
            CB_ElH.setText(heim.name);
            CB_A_Heim.setText(heim.name);
            CB_Heim.setText(heim.name);
            CB_GKHeim.setText(heim.name);
            CB_RKHeim.setText(heim.name);
            CB_ECKE_Heim.setText(heim.name);
            CB_A_Heim.setText(heim.name);
            l_TickerH.setText(heim.name);
            l_HeimName.setText(heim.name);
            b_Abseits_Heim.setText("Abseits "+ heim.name);
            //TODO Controller2.setNamen...
            CB_ElG.setText(gast.name);
            CB_A_Gast.setText(gast.name);
            CB_Gast.setText(gast.name);
            CB_GKGast.setText(gast.name);
            CB_RKGast.setText(gast.name);
            CB_ECKE_Gast.setText(gast.name);
            CB_A_Gast.setText(gast.name);
            l_TickerG.setText(gast.name);
            l_GastName.setText(gast.name);
            b_Abseits_Gast.setText("Abseits "+ gast.name);
    }

    @FXML public void resetAll(){
        AP_LinkesMenü.setDisable(true);
        TP_AktuellesMenü.setDisable(true);
        b_Menü_Fenster.setDisable(true);
        b_Menü_Menü.setDisable(true);
        stadion.schiedsrichterName="Unbekannt";
        stadion.zuschauerZahl=0;
        stadion.name="Lokales Stadion";
        gast.abseits=0;
        gast.name="Gast";
        gast.tore=0;
        gast.elfmeter=0;
        gast.auswechslungen=0;
        gast.gelbeKarten=0;
        gast.ecken=0;
        gast.roteKarten=0;
        heim.abseits=0;
        heim.name="Heim";
        heim.tore=0;
        heim.elfmeter=0;
        heim.auswechslungen=0;
        heim.gelbeKarten=0;
        heim.ecken=0;
        heim.roteKarten=0;
        timer.stop();
        pbProgress=0.00;
        pb_Spielfortschritt.setProgress(pbProgress);
        zeitMin=0;
        zeitSek=0;
        b_TimerAnhalten.setVisible(false);
        b_TM_Vorspulen.setVisible(false);
        b_TM_Zurückspulen.setVisible(false);
        tf_TM_Minuten.setVisible(false);
        printTimer();
        printTor();
        disableButtons(true);
        textHG();
        tf_AE_Spielernummer.clear();
        tf_AE_Spielername.clear();
        tf_AA_Spielername.clear();
        tf_AA_Spielernummer.clear();
        tf_GKSpielernummer.clear();
        tf_GKSpielername.clear();
        tf_RKSpielernummer.clear();
        tf_RKSpielername.clear();
        tf_TorSpielernummer.clear();
        tf_TorSpielername.clear();
        tf_TM_Minuten.clear();
        tf_Nachspielzeit_Länge.clear();
        controller2.setStadionname(stadion.name);
        controller2.setTab(1);
        setSteuerungsTab(b_Startseite,aktuellerTab,t_Startseite);
        l_TickerHInfo.setText("");
        l_TickerGInfo.setText("");
        l_TickerZeit.setText("");
        for (int i=0; i<evNr; i++){
            event[i] = null;
        }
        evNr=0;
        CB_A_Gast.setSelected(false);
        CB_A_Heim.setSelected(false);
        CB_ECKE_Gast.setSelected(false);
        CB_ECKE_Heim.setSelected(false);
        CB_RKGast.setSelected(false);
        CB_RKHeim.setSelected(false);
        CB_GelbRot.setSelected(false);
        CB_ElH.setSelected(false);
        CB_ElG.setSelected(false);
        CB_Heim.setSelected(false);
        CB_Gast.setSelected(false);
        CB_GKGast.setSelected(false);
        CB_GKHeim.setSelected(false);
        dp_SpielVb.setVisible(true);
    }
    
    
    //Formatierungshinweis: b(Button; meist eigentlich Label(oder Menüpunkt), aber aus Designgründen als Button benutzt und deswegen auch beschriftet)
    @FXML
    private Menu b_Menü_Fenster;

    @FXML
    private MenuItem b_AnzeigeÖffnen;

    @FXML
    private MenuItem b_AllesSchließen;

    @FXML
    private Menu b_Menü_Menü;

    @FXML
    private MenuItem b_MStartseite;

    @FXML
    private MenuItem b_MTor;

    @FXML
    private Menu b_MKarte;

    @FXML
    private MenuItem b_MGelbeKarte;

    @FXML
    private MenuItem b_MRoteKarte;

    @FXML
    private MenuItem b_MAuswechselung;

    @FXML
    private MenuItem b_MEcke;

    @FXML
    private MenuItem b_MElfmeter;

    @FXML
    private MenuItem b_MNachspielzeit;

    @FXML
    private Menu b_Menü_Hilfe;

    @FXML
    private MenuItem b_Über;

    @FXML
    private Font x1;

    @FXML
    private Color x2;

    @FXML
    private Label b_Startseite;

    @FXML
    private Label b_Tor;

    @FXML
    private Label b_GelbeKarte;

    @FXML
    private Label b_RoteKarte;

    @FXML
    private Label b_Auswechselung;

    @FXML
    private Label b_Ecke;

    @FXML
    private Label b_Elfmeter;

    @FXML
    private Label b_Nachspielzeit;

    @FXML
    private TabPane TP_AktuellesMenü;

    @FXML
    private Tab t_Startseite;

    @FXML
    private Label Anzeige_AktuellesMenü;

    @FXML
    private Label l_Timer;

    @FXML
    private Label l_Spielstand1;

    @FXML
    private Tab t_Tor;

    @FXML
    private Label Anzeige_AktuellesMenü1;

    @FXML
    private Font x11;

    @FXML
    private Color x21;

    @FXML
    private CheckBox CB_Heim;

    @FXML
    private CheckBox CB_Gast;

    @FXML
    private TextField tf_Spielernummer;

    @FXML
    private TextField tf_Spielername;

    @FXML
    private Button b_Tor_Eingabe;

    @FXML
    private Label Anzeige_AktuellesMenü11;

    @FXML
    private Font x111;

    @FXML
    private Color x211;

    @FXML
    private Button b_TorAnnulierung_Eingabe;

    @FXML
    private Label l_Spielstand;

    @FXML
    private Tab t_GelbeKarte;

    @FXML
    private Label Anzeige_AktuellesMenü2;

    @FXML
    private Font x12;

    @FXML
    private Color x22;

    @FXML
    private CheckBox CB_GKHeim;

    @FXML
    private CheckBox CB_GKGast;

    @FXML
    private TextField tf_GKSpielernummer;

    @FXML
    private TextField tf_GKSpielername;

    @FXML
    private Button b_GK_Eingabe;

    @FXML
    private Tab t_RoteKarte;

    @FXML
    private Label Anzeige_AktuellesMenü3;

    @FXML
    private Font x13;

    @FXML
    private Color x23;

    @FXML
    private CheckBox CB_RKHeim;

    @FXML
    private CheckBox CB_RKGast;

    @FXML
    private TextField tf_RKSpielernummer;

    @FXML
    private TextField tf_RKSpielername;

    @FXML
    private Button b_RK_Eingabe;

    @FXML
    private Tab t_Auswechselung;

    @FXML
    private Label Anzeige_AktuellesMenü4;

    @FXML
    private Font x14;

    @FXML
    private Color x24;

    @FXML
    private CheckBox CB_A_Heim;

    @FXML
    private CheckBox CB_A_Gast;

    @FXML
    private TextField tf_AA_Spielernummer;

    @FXML
    private TextField tf_AA_Spielername;

    @FXML
    private TextField tf_AE_Spielernummer;

    @FXML
    private TextField tf_AE_Spielername;

    @FXML
    private Button b_A_Eingabe;

    @FXML
    private Tab t_Ecke;

    @FXML
    private Label Anzeige_AktuellesMenü5;

    @FXML
    private Font x15;

    @FXML
    private Color x25;

    @FXML
    private CheckBox CB_ECKE_Heim;

    @FXML
    private CheckBox CB_ECKE_Gast;

    @FXML
    private Button b_Ecke_Eingabe;

    @FXML
    private Tab t_Elfmeter;

    @FXML
    private Label Anzeige_AktuellesMenü6;

    @FXML
    private Font x16;

    @FXML
    private Color x26;

    @FXML
    private Tab t_Nachspielzeit;

    @FXML
    private Label Anzeige_AktuellesMenü7;

    @FXML
    private Font x17;

    @FXML
    private Color x27;

    @FXML
    private Label Anzeige_AktuellesMenü71;

    @FXML
    private Font x171;

    @FXML
    private Color x271;

    @FXML
    private TextField tf_Nachspielzeit_Länge;

    @FXML
    private Button b_Nachspielzeit_Eingabe;

    @FXML
    private CheckBox CheckBox_Verlängerung;

    @FXML
    private Label l_Spielzeit;

    @FXML
    private Font x3;

    @FXML
    private Color x4;

    @FXML
    private Label l_NormaleZeit;

    @FXML
    private ProgressBar pb_Spielfortschritt;
    
    @FXML
    private Button b_Abseits_Heim;

    @FXML
    private Button b_Abseits_Gast;

    @FXML
    private Button b_Abseits_Annulierung;

    @FXML
    private CheckBox CB_ElH;

    @FXML
    private CheckBox CB_ElG;

    @FXML
    private Button b_El_Eingabe;

    @FXML
    private TextField sv_Zuschauerzahl;

    @FXML
    public Button b1;
    
     @FXML
    private Button b_SpielVorbereitung;

    @FXML
    private DialogPane dp_SpielVb;

    @FXML
    private TextField sv_HeimName;

    @FXML
    private TextField sv_GastName;

    @FXML
    private RadioButton sv_TestModus;

    @FXML
    private TextField sv_StadionName;

    @FXML
    private TextField sv_SchiriName;

    @FXML
    private RadioButton sv_AbseitsZählen;
    
    @FXML
    private Button b_TimerStarten;

    @FXML
    private Button b_TimerAnhalten;

     @FXML
    private Label  l_HeimName;

    @FXML
    private Label l_GastName;

    @FXML
    private Label l_StadionName;

    @FXML
    private Button b_TM_Vorspulen;

    @FXML
    private TextField tf_TM_Minuten;

    @FXML
    private Button b_Reset;

    @FXML
    private Button b_TM_Zurückspulen;
    
    @FXML
    private Label b_EventList;

    @FXML
    private Tab t_EventList;

    @FXML
    private Label l_TickerHInfo;

    @FXML
    private Label l_TickerH;

    @FXML
    private Label l_TickerGInfo;

    @FXML
    private Label l_TickerG;

    @FXML
    private Label l_TickerZeit;
    
     @FXML
    private RadioButton CB_GelbRot;

    @FXML
    private AnchorPane AP_LinkesMenü;


}
