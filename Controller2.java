package projekt;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;

import static javafx.fxml.FXMLLoader.load;

public class Controller2 {

    private Stage thisStage;

    private final Controller1 controller1;

    public Controller2(Controller1 controller1) {
        this.controller1 = controller1;
        thisStage = new Stage();
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Anzeige.fxml"));
            loader.setController(this);
            thisStage.setScene(new Scene(loader.load()));
            thisStage.setTitle("Spielstand");

            }catch (IOException e){
                e.printStackTrace();
            }
    }

    public void showStage() { thisStage.show();}

    @FXML public void setTimer(String s){
        l_ASpielzeit.setText(s);
    }
    
    @FXML public void setStadionname(String s){l_AStadionname.setText(s);}

    @FXML public void setSpielstand(String s){
        l_ASpielstand.setText(s);
    }
    
   @FXML public void setTab(Integer i) {
       System.out.println("TABSWITCH");
        switch(i) {
            case 1: TP_A.getSelectionModel().select(tA_Home); break;
            case 2: TP_A.getSelectionModel().select(tA_EckePreview); break;
            case 3: TP_A.getSelectionModel().select(tA_EckVerhältnis); break;
            case 4: TP_A.getSelectionModel().select(tA_Spielverlauf); break;
            case 5: TP_A.getSelectionModel().select(tA_Spielstatistik); break;
            case 6: TP_A.getSelectionModel().select(tA_GelbeKarte); break;
            case 7: TP_A.getSelectionModel().select(tA_RoteKarte); break;
            case 8: TP_A.getSelectionModel().select(tA_Elfmeter); break;
            case 9: TP_A.getSelectionModel().select(tA_Auswechselung); break;
            //case 10: TP_A.getSelectionModel().select(tA_Nachspielzeit/Verlängerung); break; -> TODO
            default: System.out.println("Fehler: Tab in TP_A nicht vorhanden!"); break;
        }
    }
    
    @FXML
    private TabPane TP_A;

    @FXML
    private Tab tA_Home;

    @FXML
    private Tab tA_EckePreview;

    @FXML
    private Tab tA_EckVerhältnis;

    @FXML
    private Tab tA_Spielverlauf;

    @FXML
    private Tab tA_Spielstatistik;

    @FXML
    private Tab tA_GelbeKarte;

    @FXML
    private Tab tA_RoteKarte;

    @FXML
    private Tab tA_Elfmeter;

    @FXML
    private Tab tA_Auswechselung;
    
    @FXML
    private Label l_ASpielzeit;
    
    @FXML
    private Label l_ASpielstand;
    
    @FXML
    private Label l_AStadionname;

}
